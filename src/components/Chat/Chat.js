import React, {Component} from 'react';
import './Chat.css'
import io from 'socket.io-client'
import queryString from "query-string"
import {Link} from "react-router-dom";
import ParticlesBg from 'particles-bg'

class Chat extends Component {
    constructor({location}) {
        super();
        this.data = queryString.parse(location.search)
        console.log(this.data)
        this.state = {
            name: this.data.name,
            contentConversation: [],
            message: '',


        }

    }

    scrollToBottom = () => {
        const messagesContainer = this.messagesContainer;
        messagesContainer.scrollTop = messagesContainer.scrollHeight;
    };



    componentDidMount() {

        this.socket = io("http://localhost:5000");
        this.socket.on("message", ({name, message, id}) => {
            this.setState({contentConversation: [...this.state.contentConversation, [name, message, id]],});

        });

        this.scrollToBottom();


    }

    componentDidUpdate() {
        this.scrollToBottom();

    }

    onChange = (event) => {
        let value = event.target.value;
        let name = event.target.name;
        this.setState({
            [name]: value
        })
    }
    onSend = (event) => {
        event.preventDefault()
        console.log(this.state.message)
        const {name, message, id} = this.state
        if (message !== '') {
            this.socket.emit('message', {name, message, id})
            this.setState({
                message: ''
            })
        }


    }

    render() {

        const img1 = 'http://getdrawings.com/free-icon/human-icon-png-68.png'


        const chatContent = this.state.contentConversation.map(([name, message, id], key) =>
            <div className={id === this.socket.id ? "chat-wrapper" : ''} key={key}>
                <p className={id !== this.socket.id ? " content-chat-send" : "content-chat-receive"}>

                    <img className={id === this.socket.id ? 'avatar-left' : 'avatar-right'}
                         src={img1} alt="avatar"/>
                    <span className="content-conver">
                        <span className="name-text">{name}</span>
                        <span className="msg-text">:{message}</span>
                    </span>
                </p>
            </div>)
        return (
            <div className="chat-box">
                <div className="chat-bg-name">
                    <div className="hamburger">
                        <span className="menu"></span>
                        <span className="menu"></span>
                        <span className="menu"></span>

                        <div className="logout">
                            <Link to="/" className="go-back">LogOut</Link>
                        </div>
                    </div>
                    <div>
                        <h3 className="chat-title-name">{this.state.name}</h3>
                    </div>
                </div>
                <div className="chat-title" ref={(el) => {
                    this.messagesContainer = el;
                }}>
                    {chatContent}
                </div>
                <form onSubmit={this.onSend} className="form-group">
                    <input className="chat-text"
                           placeholder="Enter Message..."
                           type="text"
                           onChange={this.onChange}
                           name="message"
                           value={this.state.message}/>
                    <button type="submit" className="button-send">Send</button>
                </form>
                <ParticlesBg type="cobweb" bg={true}/>
            </div>
        );
    }
}

export default Chat;