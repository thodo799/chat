import React, {Component} from 'react';
import './Join.css'
import {Link} from "react-router-dom";
import ParticlesBg from 'particles-bg'

class Join extends Component {
    constructor() {
        super();
        this.state = {
            name: '',
            room: '',
            statusAble: false,


        }
    }


    handleChange = (event) => {
        let target = event.target
        let value = target.value;
        let name = target.name
        this.setState({
            [name]: value
        })
    }

    handleSubmit = () => {



            if (this.state.name!=='' && this.state.room === "tho") {
                this.setState({
                   statusAble: true


                })

            }


        console.log(this.state.statusAble)
    }

componentDidUpdate(prevProps, prevState) {
        if(prevState.statusAble!==this.state.statusAble){
            this.handleSubmit()
        }
}

    render() {

        let to = `/chat?name=${this.state.name}&room=${this.state.room}`
        return (
            <div>
                <div className="joinOuterContainer">
                    <div className="joinInterContainer">
                        <h1 className="heading">Join</h1>
                    </div>
                    <div>

                        <input
                            className="joinInput"
                            placeholder="name"
                            type="text"
                            name="name"
                            onChange={this.handleChange}
                            value={this.state.name}/><br/>
                    </div>
                    <div>

                        <input
                            className="joinInput mt-20"
                            placeholder="room"
                            type="text"
                            name="room"
                            onChange={this.handleChange}
                            value={this.state.room}/><br/>
                    </div>


                    <Link to={this.state.statusAble === true ? to : '/'}
                          onClick={this.handleSubmit}>
                        <button className="button mt-20">Sign in</button>
                    </Link>

                </div>
                <ParticlesBg type="lines" bg={true}/>
            </div>

        );
    }
}

export default Join;